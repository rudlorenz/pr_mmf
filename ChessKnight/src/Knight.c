#include "Knight.h"
#include "Board.h"

int moveKnight(int* board, int boardX, int boardY, int knightX, int knightY, int count, unsigned long* callCount, int* hMove, int* vMove)
{
	*callCount = *callCount + 1;
	board[knightX * boardX + knightY] = count;

	if (count < boardX * boardY)
	{
		for (int i = 0; i < 8; i++)
		{
			if (verifyKnightPosition(knightX + vMove[i], knightY + hMove[i], boardX, boardY)
				&& (board[boardX * (knightX + vMove[i]) + knightY + hMove[i]] == 0)
				)
			{
				if (moveKnight(board, boardX, boardY, knightX + vMove[i], knightY + hMove[i], count+1, callCount, hMove, vMove)) {
					return 1;
				}
			}
		}
		board[knightX * boardX + knightY] = 0;
		return 0;
	}
	else 
	{
		return 1;
	}
}
