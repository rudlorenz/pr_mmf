#include <stdio.h>

#include "Board.h"

int verifyKnightPosition(int x, int y, int boardX, int boardY)
{
	if ((x >= 0) && (x < boardX) &&
		(y >= 0) && (y < boardY)) {
		return 1;
	}
	
	return 0;
}

int verifyBoardDimensions(int X, int Y)
{
	/* �. ���. ������� � ����������.*/
	if ((X == 1) && (Y == 1)) {
		return 1;
	}

	if ((X == 3) && ((Y == 4) || (Y >= 6))) {
		return 1;
	}

	if ((Y == 3) && ((X == 4) || (X >= 6))) {
		return 1;
	}

	if ((X == 4) && (Y >= 5)) {
		return 1;
	}

	if ((Y == 4) && (X >= 5)) {
		return 1;
	}

	if ((X >= 5) && (Y >= 5)) {
		return 1;
	}

	return 0;
}

void fillBoard(int* board, int boardX, int boardY)
{
	/* Lets use "accessibility heuristic" by classifying each cell according to
	* how accessible it is. Label each cell with number indicating from how
	* many cell each particular cell is accessible.
	* It works for square boards, lets see if it will work
	* for arbitrary rectangular board */

	/* Prefill the board */
	for (int i = 1; i < boardY - 1; ++i) {
		for (int j = 1; j < boardX - 1; j++) {
			board[boardX * i + j] = 8;
		}
	}

	for (int i = 2; i < boardX - 2; i++)
	{
		board[i] = 4;
		board[boardX*(boardY - 1) + i] = 4;
	}

	for (int i = 2; i < boardY - 2; i++)
	{
		board[boardX*i] = 4;
		board[i*boardX + boardX - 1] = 4;
	}

	board[boardX + 1] = board[boardX + boardX - 2]
		= board[boardX * (boardY - 2) + 1] = board[boardX * (boardY - 2) + boardX - 2] = 4;

	for (int i = 2; i < boardY - 2; i++)
	{
		board[boardX + i] = 6;
		board[boardX * (boardY - 2) + i] = 6;
	}

	for (int i = 2; i < boardX - 2; i++)
	{
		board[boardX * i + 1] = 6;
		board[boardX * i + boardX - 2] = 6;
	}

	/* Put 2 in corners of the board */
	board[0] = board[0 + boardX - 1] = board[boardX * (boardY - 1)] = board[boardX * (boardY - 1) + boardX - 1] = 2;

	/* Put 3 near corners */
	board[1] = board[boardX] = 3;
	board[boardX - 2] = board[boardX + boardX - 1] = 3;
	board[boardX * (boardY - 1) + 1] = board[boardX * (boardY - 2)] = 3;
	board[boardX * (boardY - 1) + boardX - 2] = board[boardX * (boardY - 2) + boardX - 1] = 3;
}

void printBoard(int* board, int boardX, int boardY)
{
	for (int i = 0; i < boardY; i++)
	{
		for (int j = 0; j < boardX; j++)
		{
			printf("%2d ", board[boardX * i + j]);
		}
		printf("\n");
	}
}