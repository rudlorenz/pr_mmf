#pragma once

/* Verify board dimensions */
/* return 0 if input dimensions are incorrect */
/* return 1 if input dimensions are correct */
int verifyBoardDimensions(int X, int Y);

/* Verify knight position */
/* return 0 if position is out of the board */
/* return 1 if position is within the board */
int verifyKnightPosition(int x, int y, int boardX, int boardY);

/* Fill board cells with numbers */
void fillBoard(int* board, int boardX, int boardY);

void printBoard(int* board, int boardX, int boardY);