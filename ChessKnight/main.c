﻿#include <stdio.h>
#include <stdlib.h>

#include "src\Board.h"
#include "src\Knight.h"


int main()
/* boardX →     boardY ↓ */
{
	int boardX = 0, boardY = 0, knightX = 0, knightY = 0;
	int *board;

	int hMove[] = { 2, 1, -1, -2, -2, -1, 1, 2 };
	int vMove[] = { -1, -2, -2, -1, 1, 2, 2, 1 };

	unsigned long call = 0;
	
	printf("Board dimensions?\n");
	scanf("%d %d", &boardX, &boardY);

	if (!verifyBoardDimensions(boardX, boardY))
	{
		printf("Knight tour is not possible.\n");
		return 0;
	}

	printf("\nKnight position?\n");
	scanf("%d %d", &knightX, &knightY);

	if (!verifyKnightPosition(knightX-1, knightY-1, boardX, boardY))
	{
		printf("Knight position is out of the board\n");
		printf("Knight tour is not possible.\n");
		return 0;
	}

	board = calloc(boardX * boardY, sizeof(int));

	if (NULL == board)
	{
		perror("calloc");
		fprintf(stderr, "Allocation error\n");
		return -1;
	}

	if (moveKnight(board, boardX, boardY, knightX-1, knightY-1, 1, &call, hMove, vMove)) {
		printf("Knight tour is possible.\n");
		printf("Call count = %lu\n\n", call);
		printBoard(board, boardX, boardY);
		printf("\n");
	}
	else {
		printf("Knight tour is not possible.\n");
	}

	free(board);

	return 0;
}