#pragma once

typedef struct tree
{
	int value;
	short height;

	struct tree* left;
	struct tree* right;
}tree;


/* Create tree node with given value */
tree* createNode(int value);

/* Node height wrapper */
short height(tree* node);

/* Get node balance */
int getBalance(tree* node);

/* Fix node height */
void fixHeight(tree* node);

tree* rotateRight(tree* node);

tree* rotateLeft(tree* node);

/* Balance tree node */
tree* balance(tree* node);

/* Insert value into AVL tree */
tree* insert(tree* head, int value);

/* Insert value into Binary tree */
tree* insertBTree(tree* head, int value);

/* Get height of a tree*/
int getHeight(tree* head);

/* Destroy tree, free memory */
void destroy(tree* head);