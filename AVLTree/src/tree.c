#include "tree.h"

#include <stdlib.h>


tree* createNode(int value)
{
	tree* node = (tree*)malloc(sizeof(tree));

	if (NULL == node) {
		return NULL;		
	}

	node->value  = value;
	node->height = 1;
	node->left   = NULL;
	node->right  = NULL;

	return node;
}


short height(tree* node)
{
	/* if node != NULL return node height. Otherwise return 0 */
	return node ? node->height : 0;
}


int getBalance(tree* node)
{
	return height(node->right) - height(node->left);
}


void fixHeight(tree* node)
{
	short rightH = height(node->right);
	short leftH  = height(node->left);
	
	if (leftH > rightH) {
		node->height = leftH + 1;
	}
	else {
		node->height = rightH + 1;
	}
}


tree* rotateRight(tree* node)
{
	tree* tmp;

	tmp = node->left;
	node->left = tmp->right;
	tmp->right = node;

	fixHeight(node);
	fixHeight(tmp);

	return tmp;
}


tree* rotateLeft(tree* node) 
{
	tree* tmp;

	tmp = node->right;
	node->right = tmp->left;
	tmp->left = node;

	fixHeight(node);
	fixHeight(tmp);
	return tmp;
}


tree* balance(tree* node)
{
	fixHeight(node);

	if (getBalance(node) == 2)
	{
		if (getBalance(node->right) < 0) {
			node->right = rotateRight(node->right);
		}
		return rotateLeft(node);
	}

	if (getBalance(node) == -2)
	{
		if (getBalance(node->left) > 0) {
			node->left = rotateLeft(node->left);
		}

		return rotateRight(node);
	}

	return node;
}


tree* insert(tree* head, int value)
{
	tree* tmp;
	if (NULL == head) {
		return createNode(value);
	}

	if (value > head->value) 
	{
		tmp = insert(head->right, value);
		if (NULL == tmp) {
			return NULL;
		}
		else {
			head->right = tmp;
		}
	}
	else /* value < head->value */
	{
		tmp = insert(head->left, value);
		if (NULL == tmp) {
			return NULL;
		}
		else {
			head->left = tmp;
		}
	}

	return balance(head);
}


tree* insertBTree(tree* head, int value)
{
	tree* tmp = head;

	if (NULL == head) 
	{
		head = createNode(value);
		return head;
	}
	else
	{
		for (;;)
		{
			while (value >= tmp->value)
			{
				if (NULL == tmp->right)
				{
					tmp->right = createNode(value); 
					
					return tmp->right ? head : NULL;
				}
				else {
					tmp = tmp->right;
				}
			}

			while (value < tmp->value)
			{
				if (NULL == tmp->left)
				{
					tmp->left = createNode(value);

					return tmp->left ? head : NULL;
				}
				else {
					tmp = tmp->left;
				}
			}

		} /* for end */
	} /* end else */
}


int getHeight(tree* head)
{
	int hleft, hright;

	if (NULL == head) {
		return 0;
	}
	
	hleft  = getHeight(head->left) + 1;
	hright = getHeight(head->right) + 1;

	/* if hleft > hright return hleft. Otherwise return hright */
	return hleft > hright ? hleft : hright;
}


void destroy(tree* head)
{
	if (NULL != head)
	{
		destroy(head->left);
		destroy(head->right);
		free(head);
	}
}

