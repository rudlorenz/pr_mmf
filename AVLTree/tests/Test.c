#include "Tests.h"

#include <stdio.h>

#include "..\src\tree.h"

#define N 1000
/* N greater then 4400 will cause stack overflow */

void test_Sequential()
{
	tree* avlTreeHead = NULL;
	tree* binTreeHead = NULL;
	tree* tmp = NULL;
	tree* tmp1 = NULL;

	for (int i = 0; i < N; ++i)
	{
		tmp = insert(avlTreeHead, i);

		if (NULL == tmp)
		{
			fprintf(stderr, "test_Sequential: insert returned NULL\nAVLTree run out of memory\n");
			destroy(avlTreeHead);
			destroy(binTreeHead);
			return;
		}
		else {
			avlTreeHead = tmp;
		}

		tmp1 = insertBTree(binTreeHead, i);

		if (NULL == tmp1)
		{
			fprintf(stderr, "test_Sequential: insertBTree returned NULL\nBinaryTree run out of memory\n");
			destroy(avlTreeHead);
			destroy(binTreeHead);
			return;
		}
		else {
			binTreeHead = tmp1;
		}

	}

	printf("test_sequential\nAVL tree height: %d\nBinary tree height: %d\n", avlTreeHead->height, getHeight(binTreeHead));

	destroy(avlTreeHead);
	destroy(binTreeHead);
}


void test_Reverse()
{
	tree* avlTreeHead = NULL;
	tree* binTreeHead = NULL;
	tree* tmp = NULL;
	tree* tmp1 = NULL;

	for (int i = N; i > 0; --i)
	{
		tmp = insert(avlTreeHead, i);

		if (NULL == tmp)
		{
			fprintf(stderr, "test_Reverse: insert returned NULL\nAVLTree run out of memory\n");
			destroy(avlTreeHead);
			destroy(binTreeHead);
			return;
		}
		else {
			avlTreeHead = tmp;
		}

		tmp1 = insertBTree(binTreeHead, i);

		if (NULL == tmp1)
		{
			fprintf(stderr, "test_Reverse: insertBTree returned NULL\nBinaryTree run out of memory\n");
			destroy(avlTreeHead);
			destroy(binTreeHead);
			return;
		}
		else {
			binTreeHead = tmp1;
		}

	}

	printf("test_Reverse\nAVL tree height: %d\nBinary tree height: %d\n", avlTreeHead->height, getHeight(binTreeHead));
	
	destroy(avlTreeHead);
	destroy(binTreeHead);
}


void test_Mixed()
{
	tree* avlTreeHead = NULL;
	tree* binTreeHead = NULL;
	tree* tmp = NULL;
	tree* tmp1 = NULL;
	int inserty = 0;

	for (int i = 0; i < N; ++i)
	{
		/* if i is even inserty = N - i */
		/* if i is odd  inserty = i */
		inserty = (i % 2 == 0) ? N - i : i;

		tmp = insert(avlTreeHead, inserty);

		if (NULL == tmp)
		{
			fprintf(stderr, "test_Mixed: insert returned NULL\nAVLTree run out of memory\n");
			destroy(avlTreeHead);
			destroy(binTreeHead);
			return;
		}
		else {
			avlTreeHead = tmp;
		}

		tmp1 = insertBTree(binTreeHead, inserty);

		if (NULL == tmp1)
		{
			fprintf(stderr, "test_Mixed: insertBTree returned NULL\nBinaryTree run out of memory\n");
			destroy(avlTreeHead);
			destroy(binTreeHead);
			return;
		}
		else {
			binTreeHead = tmp1;
		}

	}

	printf("test_Mixed\nAVL tree height: %d\nBinary tree height: %d\n", avlTreeHead->height, getHeight(binTreeHead));

	destroy(avlTreeHead);
	destroy(binTreeHead);
}


void test_Height()
{
	tree* avlTreeHead = NULL;
	tree* binTreeHead = NULL;
	
	avlTreeHead = insert(avlTreeHead, 2);
	binTreeHead = insertBTree(binTreeHead, 2);

	avlTreeHead = insert(avlTreeHead, 1);
	binTreeHead = insertBTree(binTreeHead, 1);

	avlTreeHead = insert(avlTreeHead, 3);
	binTreeHead = insertBTree(binTreeHead, 3);

	printf("test_Height\nAvl tree height: %d (getHeight = %d)\nBinary tree height: %d\n", avlTreeHead->height, getHeight(avlTreeHead), getHeight(binTreeHead));

	destroy(avlTreeHead);
	destroy(binTreeHead);
}


void test_Suit()
{
	test_Sequential();
	test_Reverse();
	test_Mixed();
	test_Height();
}
