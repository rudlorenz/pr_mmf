#pragma once

/* Create AvlTree and BinaryTree.
 * Insert keys from 1 to N.
 * Compare heights.
 * N is defined in Test.c */

/* Insert keys sequentially */
void test_Sequential();

/* Insert keys in reverse order */
void test_Reverse();

/* Insert keys in a mixed order 
 * 1, N, 2, N-1, 3, N-2, ... */
void test_Mixed();

/* Test height function */
void test_Height();

void test_Suit();