#include "PolygonArea.h"
#include <math.h>

double polygonArea(mVertex *arr, int arrLength)
{
	if (arrLength < 2) {
		return 0;
	}

	double area = 0;

	for (int i = 0; i < arrLength - 1; ++i)
	{
		area += arr[i].x * arr[i + 1].y - arr[i + 1].x * arr[i].y;
	}

	area = fabs(0.5 * area);

	return area;
}
