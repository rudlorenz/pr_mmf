#pragma once

typedef struct
{
	double x;
	double y;
} mVertex;

double polygonArea(mVertex *arr, int arrLength);