#include <stdio.h>
#include <stdlib.h>
#include "src/PolygonArea.h"

int main(int argc, char *argv[])
{
	double x = 0, y = 0, areaResult = 0;
	int arrLength = 0;

	mVertex *arr = malloc(NULL);

	/* Command line argument input */
	if (argc > 6)
	{
		/* Truncate excess argument in case user entered odd number of coordinates */
		if (0 == argc % 2) {
			argc--;
		}

		arrLength = (argc - 1) / 2;
		arr = realloc(arr, sizeof(mVertex) * arrLength);

		if (arr)
		{
			for (int i = 1; i <= arrLength; i++)
			{
				arr[i-1].x = strtod(argv[i * 2 - 1], NULL);
				arr[i-1].y = strtod(argv[i * 2], NULL);
			}

			printf("Command line args: %d\n", argc - 1);
			for (int i = 0; i < arrLength; ++i)
			{
				printf("V%d = (%.2f, %.2f)\n", i, arr[i].x, arr[i].y);
			}
		}
		else
		{
			perror("realloc");
			fprintf(stderr, "Allocation error ");
			free(arr);
			return -1;
			}
		}
	/* User input */
	else
	{
		printf("Enter Vertex Coordinates (ctrl + D to stop input)\n");

		while (scanf("%lf %lf", &x, &y) == 2)
		{
			arrLength++;
			arr = realloc(arr, arrLength * sizeof(mVertex));

			if (arr)
			{
				arr[arrLength - 1].x = x;
				arr[arrLength - 1].y = y;
			}
			else
			{
				perror("realloc");
				fprintf(stderr, "Allocation error ");
				free(arr);
				return -1;
			}
		}

		printf("\nVertex count: %d\n", arrLength);
		for (int i = 0; i < arrLength; ++i) {
			printf("V%d = (%.2f, %.2f)\n", i, arr[i].x, arr[i].y);
		}
	}

	arrLength++;
	arr = realloc(arr, sizeof(mVertex) * arrLength);
	arr[arrLength - 1].x = arr[0].x;
	arr[arrLength - 1].y = arr[0].y;

	areaResult = polygonArea(arr, arrLength);

	printf("\nPolygon area = %.2f\n", areaResult);

	free(arr);
	return 0;
}