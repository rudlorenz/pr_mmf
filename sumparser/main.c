#include <stdio.h>
#include <stdlib.h>

#include "src/parsetree.h"


int main(int argc, char **argv)
{
	FILE* fin, *fout;

	if (argc != 3)
	{
		printf("Wrong command line arguments\nUsage: %s input_file output_file\n", argv[0]);
		return -1;
	}

	fin = fopen(argv[1], "r");
	if (NULL == fin)
	{
		perror("input_file:");
		return -1;
	}
	
	fout = fopen(argv[2], "w");
	if (NULL == fout)
	{
		perror("output_file:");
		return -1;
	}

	if (AnalyzeFile(fin, fout) != 0)
	{
		fprintf(stderr, "main: AnalyzeFile result isn't 0\n");
		return -1;
	}
	
	printf("Success\n");

	fclose(fin);
	fclose(fout);
	return 0;
} 