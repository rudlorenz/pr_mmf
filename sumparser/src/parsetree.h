#pragma once

#include "list.h"
#include <stdio.h>

typedef struct parse_tree
{
	enum lex_type type;
	char* value;

	struct parse_tree* left;
	struct parse_tree* right;
}parse_tree;


parse_tree* CreateTreeNode(list_node* node);

int InsertTree(parse_tree** root, list_node* node);

void DestroyTree(parse_tree* root);

parse_tree* BuildTree(list* listptr);

void PrintTreeXML(FILE* fileptr, parse_tree* root);

int AnalyzeFile(FILE* filein, FILE* fileout);