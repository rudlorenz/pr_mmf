#include "parsetree.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "stringutils.h"
#include "lexer.h"
#include "parser.h"


parse_tree* CreateTreeNode(list_node* node)
{
	parse_tree* newNode = malloc(sizeof(parse_tree));
	if(NULL == newNode)
	{
		perror("CreateTreeNode:");
		return NULL;
	}

	newNode->value = malloc(strlen(node->value) + 1);
	if(NULL == newNode->value)
	{
		perror("CreateTreeNode:");
		return NULL;
	}
	
	memcpy(newNode->value, node->value, strlen(node->value));
	newNode->value[strlen(node->value)] = '\0';
	newNode->type = node->type;
	newNode->left  = NULL;
	newNode->right = NULL;

	return newNode;
}


int InsertTree(parse_tree** root, list_node* node)
{
	parse_tree* newRoot;

	if(NULL == node || NULL == node->value) {
		return -2;
	}

	if(NULL == *root) 
	{
		*root = CreateTreeNode(node);
		return 0;
	}

	if (node->type == OPERATOR)
	{
		newRoot = CreateTreeNode(node);
		if(NULL == newRoot)
		{
			fprintf(stderr, "InsertTree: ");
			return -1;
		}
		
		newRoot->left = *root;
		*root = newRoot;
		return 0;
	}

	if (node->type == NUMBER)
	{
		if ((*root)->left == NULL)
		{
			(*root)->left = CreateTreeNode(node);
			
			if(NULL == (*root)->left)
			{
				fprintf(stderr, "InsertTree: ");
				return -1;
			}

			return 0;
		}
		else
		{
			(*root)->right = CreateTreeNode(node);

			if (NULL == (*root)->right)
			{
				fprintf(stderr, "InsertTree: ");
				return -1;
			}

			return 0;
		}
	}

	return -2;
}


void DestroyTree(parse_tree* root)
{
	if (NULL != root)
	{
		DestroyTree(root->left);
		DestroyTree(root->right);
		free(root->value);
		free(root);
	}
}


parse_tree* BuildTree(list* listptr)
{
	parse_tree* root = NULL;
	list_node* cur = NULL;
	int result = 0;

	if (NULL == listptr || NULL == listptr->head) 
	{
		fprintf(stderr, "BuildTree: listptr is NULL\n");
		return NULL;
	}

	cur = listptr->head;
	while (NULL != cur && result == 0)
	{
		result = InsertTree(&root, cur);
		cur = cur->next;
	}

	return root;
}


void PrintTree(FILE* fileptr, parse_tree* root, int spacenum)
{
	if(NULL != root)
	{
		fprintf(fileptr, "%*c<node type = ", spacenum, ' ');
		if (root->type == NUMBER) {
			fprintf(fileptr, "\"NUMBER\" ");
		}
		else {
			fprintf(fileptr, "\"OPERATOR\" ");
		}
		fprintf(fileptr, "value = \"%s\">\n", root->value);

		PrintTree(fileptr, root->left, spacenum+4);
		PrintTree(fileptr, root->right, spacenum+4);

		fprintf(fileptr, "%*c</node>\n", spacenum, ' ');
	}
}


void PrintTreeXML(FILE* fileptr, parse_tree* root)
{
	if (NULL == fileptr || NULL == root) 
	{
		fprintf(stderr, "PrintTreeXML: received NULL\n");
		return;
	}

	fprintf(fileptr, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	fprintf(fileptr, "<tree>\n");
	PrintTree(fileptr, root, 4);
	fprintf(fileptr, "</tree>\n");

}

int AnalyzeFile(FILE* filein, FILE* fileout)
{
	char *tmp = NULL, *trunctmp = NULL;
	int tmplen = 0;
	list* listptr = NULL;
	parse_tree* root = NULL;

	if (NULL == filein || NULL == fileout)
	{
		fprintf(stderr, "AnalyzeFile: file is NULL\n");
		return -1;
	}

	if (ReadLine(&tmp, &tmplen, filein) != 0)
	{
		fprintf(stderr, "AnalyzeFile: ReadLine returned not null\n");
		return -1;
	}
	
	RemoveNonGraphs(&tmp, &tmplen);

	trunctmp = TruncateStringMemory(&tmp, tmplen);

	if (NULL == trunctmp)
	{
		fprintf(stderr, "AnalyzeFile: TruncateStringMemory returned NULL\n");
		free(tmp);
		return -1;
	}

	listptr = ParseIntoList(trunctmp, tmplen);
	if (NULL == listptr)
	{
		fprintf(stderr, "AnalyzeFile: ParseIntoList returned NULL\n");
		free(trunctmp);
		return -1;
	}

	if (ParseList(listptr) == 0)
	{
		fprintf(stderr, "AnalyzeFile: ParseList: Wrong input lexems\n");
		free(trunctmp);
		DestroyList(listptr);
		return -1;
	}

	root = BuildTree(listptr);
	if (NULL == root)
	{
		fprintf(stderr, "AnalyzeFile: BuildTree returned NULL\n");
		free(trunctmp);
		DestroyList(listptr);
		return -1;
	}

	DestroyList(listptr);

	PrintTreeXML(fileout, root);

	DestroyTree(root);
	free(trunctmp);

	return 0;
}
