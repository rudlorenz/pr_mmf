#include "parser.h"

#include <stdlib.h>

int ParseNUMBER(list_node* node)
{
	if (node->type == NUMBER)
	{
		if (node->next != NULL) {
			return ParseOPERATOR(node->next);
		}
		else {
			return 1;
		}
	}
	else {
		return 0;
	}
}


int ParseOPERATOR(list_node* node)
{
	if (node->type == OPERATOR)
	{
		if (node->next != NULL) {
			return ParseNUMBER(node->next);
		}
		else {
			return 0;
		}
	}
	else {
		return 0;
	}
}


int ParseList(list* listptr)
{
	if(NULL == listptr || NULL == listptr->head) {
		return 0;
	}
	
	return ParseNUMBER(listptr->head);
}