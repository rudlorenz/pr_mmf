#include "lexer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int IsOperator(char c)
{
	if (c == '+' || c == '-') {
		return 1;
	}
	else {
		return 0;
	}
}


list* ParseIntoList(char* string, int stringlen)
{
	char* start = string;
	char* cur = string;
	list* listptr = NULL;
	char* buf = malloc(stringlen+1);

	if (NULL == string || 0 == stringlen)
	{
		free(buf);
		return NULL;
	}

	if (NULL == buf)
	{
		perror("ParseIntoList: malloc returned NULL");
		return NULL;
	}

	while (*cur != 0)
	{
		if (isdigit(*cur))
		{
			start = cur;
			while (isdigit(*cur)) {
				cur++;
			}

			memcpy(buf, start, cur - start);
			buf[cur - start] = '\0';
			if (InsertList(&listptr, NUMBER, buf) != 0)
			{
				fprintf(stderr, "ParseIntoList: InsertList error\n");
				DestroyList(listptr);
				free(buf);
				return NULL;
			}
		}
		else if (IsOperator(*cur))
		{
			memcpy(buf, cur, 1);
			buf[1] = '\0';

			if (InsertList(&listptr, OPERATOR, buf) != 0)
			{
				fprintf(stderr, "ParseIntoList: InsertList error\n");
				DestroyList(listptr);
				free(buf);
				return NULL;
			}

			cur++;
		}
		else /* dirty  */
		{
			fprintf(stderr, "ParseIntoList: Unacceptable character: %c (at position %d)\n", *cur, cur - string + 1);
			DestroyList(listptr);
			free(buf);
			return NULL;		
		}
	}

	free(buf);
	return listptr;
}