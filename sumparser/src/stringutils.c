#include "stringutils.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>


int ReadLine(char** stringptr, int* stringlen, FILE* instream)
{
	int aread = CHUNK;
	int stringsize = 0;
	char* buf = malloc(CHUNK);
	char* tmp;

	if (NULL == buf) 
	{
		perror("ReadLine: malloc");
		return -1;
	}

	if (NULL == instream) {
		return -1;
	}

	while (aread == CHUNK)
	{
		aread = fread(buf, sizeof(char), CHUNK, instream);

		stringsize += CHUNK;
		tmp = realloc(*stringptr, stringsize);

		if (NULL == tmp)
		{
			perror("ReadLine: realloc");
			free(buf);
			free(*stringptr);
			*stringptr = NULL;
			return -1;
		}

		*stringptr = tmp;

		memcpy(*stringptr + (stringsize - CHUNK), buf, aread);
	}

	*stringlen = stringsize - CHUNK + aread;
	free(buf);
	return 0;
}


void RemoveNonGraphs(char** string, int* strlen)
{
	char* i = *string;
	char* j = *string;
	int len = 0, aclen = 0;

	while (len < *strlen) 
	{
		*j = *i++;
		len++;
		if (isgraph(*j)) {
			j++;
			aclen++;
		}
	}
	*strlen = aclen;
	*j = '\0';
}


char* TruncateStringMemory(char** string, int strlen)
{
	char* newstring = malloc(strlen+1);

	if (NULL == *string || 0 == strlen) 
	{
		fprintf(stderr, "TruncateStringMemory: input string is empty\n");
		return NULL;
	}
	
	if (NULL == newstring)
	{
		perror("TruncateStringMemory: malloc");
		return NULL;
	}

	/* strlen+1 to copy \0 character */
	memcpy(newstring, *string, strlen+1);
	free(*string);
	*string = NULL;

	return newstring;
}