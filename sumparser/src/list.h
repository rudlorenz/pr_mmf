#pragma once

enum lex_type {OPERATOR, NUMBER};

typedef struct list_node
{
	enum lex_type type;
	char* value;
	struct list_node* next;
	struct list_node* fwd;
}list_node;

typedef struct list
{
	list_node* head;
	list_node* tail;
}list;

list_node* CreateNode(enum lex_type type, char* value);

int InsertList(list** listptr, enum lex_type type, char* value);

void DestroyList(list* head);

void PrintList(list* head);

void PrintListBackwards(list* head);