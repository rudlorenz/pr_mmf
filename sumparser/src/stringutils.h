#pragma once

#include <stdio.h>

#define CHUNK 100


/* Copy contents from STREAM to STRINGPTR return stringptr length */
int ReadLine(char** stringptr, int* stringlen, FILE* instream);

/* Remove non graphical characters from string */
void RemoveNonGraphs(char** string, int* strlen);

char* TruncateStringMemory(char** string, int strlen);
