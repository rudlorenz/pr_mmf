#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


list_node* CreateNode(enum lex_type type, char* value)
{
	list_node* newNode = malloc(sizeof(list_node));
	if (NULL == newNode)
	{
		perror("CreateNode: malloc");
		return NULL;
	}

	newNode->value = malloc(strlen(value)+1);
	if (NULL == newNode->value)
	{
		perror("CreateNode: malloc");
		return NULL;
	}

	newNode->type = type;
	newNode->next = NULL;
	newNode->fwd = NULL;

	strcpy(newNode->value, value);

	return newNode;
}

int InsertList(list** listptr, enum lex_type type, char* value)
{
	list_node* tmp;

	if (NULL == (*listptr)) 
	{
		(*listptr) = malloc(sizeof(list));
		if (NULL == (*listptr))
		{
			perror("InsertList: malloc returned NULL");
			return -1;
		}

		(*listptr)->head = CreateNode(type, value);
		(*listptr)->tail = (*listptr)->head;

		return 0;
	}

	/* Assuming head->next equals NULL and tail is NULL too */
	if (NULL == (*listptr)->head)
	{
		(*listptr)->head = CreateNode(type, value);
		(*listptr)->tail = (*listptr)->head;

		return 0;
	}

	(*listptr)->tail->next = CreateNode(type, value);

	if (NULL == (*listptr)->tail)
	{
		perror("InsertList: CreateNode returned NULL");
		DestroyList(*listptr);
		return -1;
	}

	tmp = (*listptr)->tail;
	(*listptr)->tail = (*listptr)->tail->next;
	(*listptr)->tail->fwd = tmp;

	return 0;
}

void DestroyList(list* listptr)
{
	list_node* cur = NULL;

	while (listptr->head!= NULL)
	{
		cur = listptr->head->next;
		free(listptr->head->value);
		free(listptr->head);
		listptr->head= cur;
	}
	free(listptr);
}

void PrintList(list* listptr)
{
	list_node* cur = listptr->head;
	int i = 0;

	while (cur != NULL) 
	{
		printf("%3d | type: ", i++);
		if (cur->type == OPERATOR) {
			printf("OPERATOR;");
		}
		else {
			printf("NUMBER;  ");
		}
		printf("value %s\n", cur->value);
		cur = cur->next;
	}
}


void PrintListBackwards(list* listptr)
{
	list_node* cur = listptr->tail;
	int i = 0;

	while (cur != NULL)
	{
		printf("%3d | type: ", i++);
		if (cur->type == OPERATOR) {
			printf("OPERATOR;");
		}
		else {
			printf("NUMBER;  ");
		}
		printf("value %s\n", cur->value);
		cur = cur->fwd;
	}
}