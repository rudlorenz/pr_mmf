# Programming workshop

2017, Department of Mathematics and Mechanics NSU.
First course, second semester.

rudlorenz@gmail.com

## Main task list

    1. 7.4-7 due 03.03.2017  - Polygon Area
    2. 7.5-22 due 17.03.2017 - Chess Knight
    3. 7.1-48 due 07.04.2017 - AVL Tree
    4. 7.2-85 due 28.04.2017 - Sum Parser
    5. 7.3-57 due 19.05.2017 - Term Parser

You can find task description here:
<http://e-lib.nsu.ru/reader/bookView.html?params=UmVzb3VyY2UtOTQ5/cGFnZTAwMQ&q=%CA%E0%F1%FC%FF%ED%EE%E2>