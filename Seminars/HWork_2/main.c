#include <stdio.h>

#define N 8
#define STR_LENGHT N%4 + 7
#define FRAME_CHAR 'I'

int main()
{
	int i;
	char s = 0;
	char str[STR_LENGHT] = { 0 };

	scanf(" %c", &s);
	
	for (int i = 0; i < STR_LENGHT; i++) {
		str[i] = s;
	}

	str[0] = str[STR_LENGHT - 2] = FRAME_CHAR;

	str[STR_LENGHT-1] = '\0';

	printf("\n%s\n", str);

	return 0;
}