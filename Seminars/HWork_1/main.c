#include <stdio.h>
#include <string.h>

int main()
{
	char a[50], b[50];
	int minLength, count = 0;
	
	fgets(a, 49, stdin);
	fgets(b, 49, stdin);

	minLength = (strlen(a) < strlen(b)) ? strlen(a) : strlen(b);

	for (int i = 0; i < minLength; ++i)
	{
		if (a[i] == b[i] && a[i] == '9') {
			count++;
		}
	}

	printf("A is %s\n", a);
	printf("B is %s\n", b);
	printf("count is %d\n", count);

	return 0;
}